import readline from 'readline/promises';
<<<<<<< HEAD
import { board, myBoard, createEnemyShipCoordinates, createOwnShips, randomCoords, myShipsObjectList } from './shipcreation.js';
import { shot, myHits, CPUHits, CPUshot, isValidCoordinates, alreadyShot } from './shot.js';
import { renderBoard, renderMyBoatsToBoard, createEmptyBoard, renderBoard2 } from './boards.js';
=======
import { createEnemyShipCoordinates, createOwnShips, randomCoords, myShipsObjectList, enemyShips } from './shipcreation.js';
import { shot, myHits, CPUHits, CPUshot, isValidCoordinates, alreadyShot, message } from './shot.js';
import { renderBoard, renderMyBoatsToBoard, createEmptyBoard } from './boards.js';
>>>>>>> 22cfe9a0f7c6540abd73a6851e530cf22aa2ba28

const ownShotCoordinates = [];
const CPUShotCoordinates = [];
// ----------- configure area - amount of ships and board--------
export const maxGameAreaRows = 5
export const maxGameAreaCols = 5
export const cruiserAmount = 1
export const boats = 3
export const preventShipsTouching = true // set this to true, so that ships can't be in a group 
export const totalShips = cruiserAmount*3+boats
const logObjects = false // if set true, at the end of the game all boat objects are logged (after 10s delay)
// --------------------------------------------------------------

async function readInput() {
    const action = readline.createInterface(process.stdin, process.stdout);
    let notQuit = true;
    let cpuTurn = false;
    let myHit = false; // my shot was a hit and I can shoot again
    let CPUhit = false; // CPU's shot was a hit and it can shoot again

    const printObjects = () => {
        console.log("Enemy ships status at the end of the game:", enemyShips)
        console.log("My ships status at the end of the game:", myShipsObjectList)
    }

    while ((notQuit) && (myHits < totalShips) && (CPUHits < totalShips)) {
        if (!cpuTurn) {
            let actionRequest = await action.question("\x1b[35m\nGive me coordinates where you want to shoot or type quit: \x1b[0m");
            let coordinates = actionRequest.toUpperCase();
            let row = coordinates.charCodeAt(0) - 64; // Convert letter to row index
            let col = parseInt(coordinates.charAt(1));

<<<<<<< HEAD
        renderBoard();
        if (myTurn) {
=======
>>>>>>> 22cfe9a0f7c6540abd73a6851e530cf22aa2ba28
            if (coordinates === "QUIT") {
                console.log("\x1b[35mGame Over!\x1b[0m");
                notQuit = false;
                break;
            }

            if (isValidCoordinates(row, col, coordinates)) {
                if (isValidCoordinates(row, col, coordinates) || myHit) {
                    ownShotCoordinates.push(coordinates);
                    if (!alreadyShot(row, col, board)) {
                        myHit = shot(actionRequest);
                        if (myHit) {
                            renderBoard();
                            continue;
                        } else {
                            cpuTurn = true;
                        }
                    } else {
                        shot(actionRequest);
                    }
                } else {
                    cpuTurn = true;
                }
            }
        }

        if (cpuTurn && (myHits < totalShips)) {
            function CPUsTurn() {
<<<<<<< HEAD
                let CPUShotCoordinate = randomCoords(5,5); //letterCoordinate + numberCoordinate;
                if (CPUShotCoordinates.includes(CPUShotCoordinate)) {
                    CPUsTurn();
                } else {
                    CPUshot(CPUShotCoordinate);
=======
                let CPUShotCoordinate = randomCoords(maxGameAreaRows, maxGameAreaCols); //letterCoordinate + numberCoordinate;
                if (CPUShotCoordinates.includes(CPUShotCoordinate)) {
                    CPUsTurn();
                } else {
                    CPUhit = CPUshot(CPUShotCoordinate);
>>>>>>> 22cfe9a0f7c6540abd73a6851e530cf22aa2ba28
                    CPUShotCoordinates.push(CPUShotCoordinate);
                    if (CPUhit) {
                        renderBoard();
                        cpuTurn = true;
                        console.log(message);
                    } else {
                        renderBoard();
                        cpuTurn = false;
                        console.log(message);
                    }
                }
            }
            CPUsTurn();
        }
    } action.close();
    if (logObjects) setTimeout(printObjects , 10000) // if row is active, at the end of the game game objects are logged with 10s delay at the end of the game
}

<<<<<<< HEAD
function gameInit() {
    
    renderBoard2(munLauta);
    // createEnemyShipCoordinates(board);
    // createOwnShips(myBoard);
    createOwnShips(munLauta);
    renderMyBoatsToBoard(myShipsObjectList);
    renderBoard2(munLauta);
    // console.clear();
    // renderBoard();
    // readInput();
}

export let munLauta = createEmptyBoard(8, 8)
=======
async function gameInit() {
    createEnemyShipCoordinates(board);
    createOwnShips(myBoard);
    renderMyBoatsToBoard(myShipsObjectList);
    renderBoard();
    readInput();
}

export const board = createEmptyBoard(maxGameAreaRows, maxGameAreaCols);
export const myBoard = createEmptyBoard(maxGameAreaRows, maxGameAreaCols);

>>>>>>> 22cfe9a0f7c6540abd73a6851e530cf22aa2ba28
gameInit();