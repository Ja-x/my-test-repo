import { Ship } from "./classes.js";
import { maxGameAreaRows, maxGameAreaCols, cruiserAmount, totalShips, preventShipsTouching } from "./battleship.js"

export let enemyShipsCoordinates = [];
export const myShipsObjectList = [];
export const enemyShips = []; // list of shipObjects taking their space at the board, has {coords, type, sunken, near}
export let myShipsCoordinates = [];
let ownCruisersCreated = 0;
let enemyCruisersCreated = 0;

export function randomCoords(X, Y) {
    const randomNumberX = Math.ceil(Math.random() * (X));
    const randomNumberY = Math.ceil(Math.random() * (Y));
    const numberInStr = String.fromCharCode(randomNumberY + 48)
    const rowletter = String.fromCharCode(randomNumberX + 64)
    const coordinate = rowletter + numberInStr;
    return coordinate;
}

export function createEnemyShipCoordinates(board) {

    while (enemyCruisersCreated < cruiserAmount) {
        const cruiseShipOrientation = Math.ceil(Math.random() * 2);
        if (cruiseShipOrientation < 2) {
            const shipCoordinate1 = randomCoords(maxGameAreaRows, maxGameAreaCols - 2);
            if (createHorizontalCruiser(shipCoordinate1, "enemy")) {
                enemyCruisersCreated++;
            }
        } else {
            const shipCoordinate = randomCoords(maxGameAreaRows - 2, maxGameAreaCols);
            if (createVerticalCruiser(shipCoordinate, "enemy")) {
                enemyCruisersCreated++;
            }
        }
    }

    while (enemyShips.length < totalShips) {
        let shipCoordinate = randomCoords(maxGameAreaRows, maxGameAreaCols);
        let Conflict = isInTheObjectList(shipCoordinate, "enemy") //true;  this is used to check that we don't re-use already used coordinates = put 2 ships to the same coords

        if (!Conflict) { enemyShips.push(new Ship(shipCoordinate, "boat")); }
    }

    for (let vessel = 0; vessel < enemyShips.length; vessel++) {
        enemyShipsCoordinates.push(enemyShips[vessel].coords)
    }
<<<<<<< HEAD
    //console.log("enemyShipsCoords: ", enemyShips);
=======
    // console.log("enemyShips: ", enemyShipsCoordinates); // print these for debugging purposes
>>>>>>> 22cfe9a0f7c6540abd73a6851e530cf22aa2ba28
}

export function createOwnShips() {

    while (ownCruisersCreated < cruiserAmount) {
        const cruiseShipOrientation = Math.ceil(Math.random() * 2);
        if (cruiseShipOrientation < 2) {
            const shipCoordinate1 = randomCoords(maxGameAreaRows, maxGameAreaCols - 2);
            if (createHorizontalCruiser(shipCoordinate1, "my")) {
                ownCruisersCreated++
                //console.log("One horizontal cruiser created to",shipCoordinate1)
            }
        } else {
            const shipCoordinate = randomCoords(maxGameAreaRows - 2, maxGameAreaCols);
            if (createVerticalCruiser(shipCoordinate, "my")) {
                ownCruisersCreated++
                //console.log("One vertical cruiser created to",shipCoordinate)
            }
        }
    }

    while (myShipsObjectList.length < totalShips) {
        createShip("my")
    }

    for (let vessel = 0; vessel < myShipsObjectList.length; vessel++) {
        myShipsCoordinates.push(myShipsObjectList[vessel].coords)
    }
<<<<<<< HEAD
    console.log("omat laivat:", myShipsObjectList)
    // console.log("myShipsCoordinates:", myShipsCoordinates)
=======
>>>>>>> 22cfe9a0f7c6540abd73a6851e530cf22aa2ba28
}

function createShip(whose) {

    const shipCoordinate = randomCoords(maxGameAreaRows, maxGameAreaCols)
    if (whose === "my") {
        if (!isInTheObjectList(shipCoordinate, "my")) {
            myShipsObjectList.push(new Ship(shipCoordinate, "boat"))
            // console.log("Ship created to",shipCoordinate)
        }
    } else {
        if (!isInTheObjectList(shipCoordinate, "enemy")) {
            enemyShips.push(new Ship(shipCoordinate, "boat"))
        }
    }
}

function createHorizontalCruiser(coords, whose) {
    let nr = coords.substr(1, 1)
    nr = Number(nr)
    let nr1 = nr + 1
    let nr2 = nr + 2
    const ship1 = new Ship(coords, "cruiser")
    const coords2 = coords.substr(0, 1) + nr1
    const ship2 = new Ship(coords2, "cruiser")
    const coords3 = coords.substr(0, 1) + nr2
    const ship3 = new Ship(coords3, "cruiser")


    if (whose === "my") {
        for (let looppi = 0; looppi < myShipsObjectList.length; looppi++) {
            if (myShipsObjectList[looppi].near.includes(coords) || myShipsObjectList[looppi].near.includes(coords2) || myShipsObjectList[looppi].near.includes(coords3)) {
                return false
            }
        }
        myShipsObjectList.push(ship1);
        myShipsObjectList.push(ship2);
        myShipsObjectList.push(ship3);
        return true;
    } else if (whose === "enemy") {
        for (let looppi = 0; looppi < enemyShips.length; looppi++) {
            if ((preventShipsTouching) && (enemyShips[looppi].near.includes(coords) || enemyShips[looppi].near.includes(coords2) || enemyShips[looppi].near.includes(coords3))) {
                return false;
            } else if (enemyShips[looppi].coords.includes(coords) || enemyShips[looppi].coords.includes(coords2) || enemyShips[looppi].coords.includes(coords3)) {
                return false;
            }
        }
        enemyShips.push(ship1);
        enemyShips.push(ship2);
        enemyShips.push(ship3);
        return true
    } else { console.log("error 247"); return false; }

}

function createVerticalCruiser(coords, whose) { // maxCoords is number indicating max length of a single row / col
    let nr = coords[1] //coords.substr(1, 1)
    let rowLetter = coords.charCodeAt(0);
    let letter2 = String.fromCharCode(rowLetter + 1)
    let letter3 = String.fromCharCode(rowLetter + 2)

    const ship1 = new Ship(coords, "cruiser")
    const coords2 = letter2 + nr
    const ship2 = new Ship(coords2, "cruiser")
    const coords3 = letter3 + nr
    const ship3 = new Ship(coords3, "cruiser")

    if (whose === "my") {
        for (let looppi = 0; looppi < myShipsObjectList.length; looppi++) {
            if ((preventShipsTouching) && (myShipsObjectList[looppi].near.includes(coords) || myShipsObjectList[looppi].near.includes(coords2) || myShipsObjectList[looppi].near.includes(coords3))) {
                return false;
            } else if (myShipsObjectList[looppi].coords.includes(coords) || myShipsObjectList[looppi].coords.includes(coords2) || myShipsObjectList[looppi].coords.includes(coords3)) {
                return false;
            }
        }
        myShipsObjectList.push(ship1);
        myShipsObjectList.push(ship2);
        myShipsObjectList.push(ship3);
        return true;
    } else if (whose === "enemy") {
        for (let looppi1 = 0; looppi1 < enemyShips.length; looppi1++) {
            if ((preventShipsTouching) && (enemyShips[looppi1].near.includes(coords) || enemyShips[looppi1].near.includes(coords2) || enemyShips[looppi1].near.includes(coords3))) {
                return false;
            } else if (enemyShips[looppi1].coords.includes(coords) || enemyShips[looppi1].coords.includes(coords2) || enemyShips[looppi1].coords.includes(coords3)) {
                return false;
            }
        }
        enemyShips.push(ship1);
        enemyShips.push(ship2);
        enemyShips.push(ship3);
        return true
    } else { console.log("error 248"); return false; }
}


function isInTheObjectList(coords, whose) {
    if (whose === "enemy") {
        let comparisonList = enemyShips.map(ships => ships);
        for (let ship = 0; ship < comparisonList.length; ship++) {
<<<<<<< HEAD
            //if (coords === comparisonList[ship].coords) {
                if (comparisonList[ship].near.includes(coords)) {
                return true
=======
            if (preventShipsTouching) {
                if (comparisonList[ship].near.includes(coords)) {
                    // console.log("***blocked", coords, whose)
                    return true
                }
            } else {
                if (coords === comparisonList[ship].coords) {
                    return true
                }
>>>>>>> 22cfe9a0f7c6540abd73a6851e530cf22aa2ba28
            }
        }
    } else if (whose === "my") {
        let comparisonList = myShipsObjectList.map(ships => ships);
        for (let ship = 0; ship < comparisonList.length; ship++) {
<<<<<<< HEAD
            // if (coords === comparisonList[ship].coords) {
               if (comparisonList[ship].near.includes(coords)) {
                return true
=======
            if (preventShipsTouching) {
                if (comparisonList[ship].near.includes(coords)) {
                    // console.log(" *my* blocked:", coords, whose)
                    return true
                }
            } else {
                if (coords === comparisonList[ship].coords) {
                    return true
                }
>>>>>>> 22cfe9a0f7c6540abd73a6851e530cf22aa2ba28
            }
        }
    }
    return false
}
