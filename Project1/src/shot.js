import { enemyShipsCoordinates, enemyShips, myShipsCoordinates, myShipsObjectList } from "./shipcreation.js";
import { renderBoard } from "./boards.js";
import { board, myBoard, maxGameAreaCols, maxGameAreaRows } from "./battleship.js"
export let myHits = 0; // Counter for the number of hits by the player
export let CPUHits = 0; // Counter for the number of hits by the CPU
export let message = "";

let sunkenEnemyCruiser = 0; // Counter for the number of sunken enemy cruisers
let mySunkenCruiser = 0; // Counter for the number of sunken cruisers

export function isValidCoordinates(row, col, coordinates) { // Function to check if the coordinates are valid
    if (!(row >= 1 && row <= maxGameAreaRows && col >= 1 && col <= maxGameAreaCols)) {
        console.clear();
        console.log(`\x1b[33m'${coordinates}' is an invalid coordinate!\n\x1b[0m`);
        renderBoard();
        return;
    } else {
        return row >= 1 && row <= maxGameAreaRows && col >= 1 && col <= maxGameAreaCols;
    }
}

export function alreadyShot(row, col, board) { // Check if the coordinates have already been shot
    return board[row][col] === "X" || board[row][col] === ".";
}

function markShot(row, col, shotCoordinates, board) { // Mark the shot on the board
    if (isHit(row, col, shotCoordinates, board)) {
        board[row][col] = "X"; // Mark the shot as a hit
    } else {
        board[row][col] = "."; // Mark the shot as a miss
    }
}

function isHit(row, col, shotCoordinates, board) { // Function to check if a shot is a hit
    const targetCoordinate = String.fromCharCode(64 + row) + (col + 0); // Convert row and col to coordinates like 'A1'
    return shotCoordinates.includes(targetCoordinate);
}

export function shot(input) { // Function to take a shot
    let coordinates = input.toUpperCase(); // Convert input to uppercase
    let row = coordinates.charCodeAt(0) - 64; // Convert letter to row index
    let col = parseInt(coordinates.charAt(1));

    console.clear(); // Clear the console

    if (alreadyShot(row, col, board)) { // Check if a shot has already been taken at the coordinates
        console.log(`\x1b[33mYou have already shot ${coordinates}\n\x1b[0m`);
        renderBoard();
        return;
    }

    markShot(row, col, enemyShipsCoordinates, board); // Mark the shot on the board

    let hit = isHit(row, col, enemyShipsCoordinates, board); // Check if the shot is a hit

    if (hit) { // If the shot is a hit
        for (let index = 0; index < enemyShips.length; index++) {
            if (enemyShips[index].coords === coordinates) {
                enemyShips[index].sunken = true;
                if (enemyShips[index].type === "cruiser") {
                    sunkenEnemyCruiser++;
                    if (sunkenEnemyCruiser === 3) {
                        console.log("\x1b[32mYou sunk the enemy cruiser!\x1b[0m\n");
                    } else { console.log("\x1b[32mYou hit the enemy cruiser!\x1b[0m\n"); }
                } else {
                    console.log("\x1b[32mYou have sunk an enemy boat.\x1b[0m\n");
                }
            }
        }
        myHits++; // Increment the hit counter

    } else { // If the shot is a miss
        console.log("You missed!\n");
    }
    if (myHits === enemyShips.length) { // If all enemy boats are sunk
        console.log("\x1b[32mYou have sunk all the enemy boats! You win!\x1b[0m\n");
    }
    return hit;
}

export function CPUshot(input) { // Function to take a shot
    let coordinates = input.toUpperCase();
    let row = coordinates.charCodeAt(0) - 64; // Convert letter to row index
    let col = parseInt(coordinates.charAt(1));

    markShot(row, col, myShipsCoordinates, myBoard); // Mark the shot on the board

    let hit = isHit(row, col, myShipsCoordinates, myBoard); // Check if the shot is a hit

    if (hit) { // If the shot is a hit
        for (let index = 0; index < myShipsObjectList.length; index++) {
            if (myShipsObjectList[index].coords === coordinates) {
                myShipsObjectList[index].sunken = true;
                if (myShipsObjectList[index].type === "cruiser") {
                    mySunkenCruiser++;
                    if (mySunkenCruiser === 3) {
                        message = "\x1b[31mThe enemy sunk your cruiser!\x1b[0m\n";
                    } else { message = "\x1b[31mThe enemy hit your cruiser!\x1b[0m\n"; }

                } else {
                    message = "\x1b[31mThe enemy has sunk your boat.\x1b[0m\n";
                }
            }
        }

        CPUHits++; // Increment the hit counter

    } else { // If the shot is a miss
        message = "\nThe enemy missed!";
    }

    if (CPUHits === myShipsObjectList.length) { // If all your boats are sunk
        message = "\n\x1b[31mThe enemy has sunk all your boats! You lose.\x1b[0m";
    } return hit;
}