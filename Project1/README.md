Project Battleship
This project has been divided to multiple stages. For you to get most out of this project, do not optimize for further stages. Always assume that the stage you're currently working with is the final stage. Further reading about the reasoning behind this: Wikipedia: [You aren't gonna need it]

In this project you are going to build a command line game "Battleship". 

Stage 1: Single player BattleBoat
Create a single player battleship game. The game is played on a 5x5 grid. The enemy has five (5) boats that each occupy a single square. When the game starts, the game board is rendered to the console. The empty squares where the player has not shot yet are marked with ~. The player is prompted to give the coordinated for the next attack. The coordinates are entered in a single command consisting of first the letter and then the number part of the coordinates. For example, the player can shoot to A1, B3, E5, etc. The letters range from A to E and the numbers from 1 to 5.

If the player tries to shoot outside the board, or gives invalid coordinates, the game prints message "Invalid coordinates", and the game continues.

If the player hits an enemy ship, the game prints "You hit an enemy boat!". The game board is rendered again, showing X where the player shot. If all the enemy boats have been hit, a message "You have sunk all the enemy ships!" is printed, and the game ends. Otherwise the game continues.

If the player misses, the game prints "You missed!". The game board is rendered again, showing . where the player shot, and the game continues.

If the player tries to shoot a square where they have already shot, a message "You have already shot <square coordinages>" is printed, and the game continues.

If the player enters the command "quit", a "Game Over!" message is printed, and the game ends.

Stage 2: Single player Mini Battleship
In this stage the enemy has a Cruiser in addition to the five boats. A cruiser occupies three squares, and can be placed either horizontally or vertically, but not diagonally. 

When the game starts, all the enemy vessels are generated to random locations. Make sure that the vessels are not on top of each other, and that the cruiser fits to the game board.

If a player hits a cruiser, game prints "You hit the enemy cruiser". If the player has hit all the enemy cruiser squares, the game prints "You have sunk the enemy cruiser". If the player hits an enemy boat, the game prints "You have sunk an enemy boat".

Otherwise the game mechanics remain the same.

Stage 3: Multiplayer Battleship
Modify your game so that it can be played against a computer.

When the game starts, both the player and the computer get their own boards generated. The player sees the enemy board just as in the previous stage. In addition the player sees their own board, which shows S in each square that is occupied by a boat, and C by an every square occupied by a cruiser.

Player starts. If either a player or the computer hits a ship, they continue shooting until they miss.
When one of the players sinks all the opponents ships, the game ends.

Extra Challenge 1: Instead of drawig the player and enemy boards one after each other, draw them side by side.
Extra Challenge 2: Develop your enemy logic so that when it hits the cruiser, it keeps shooting to the nearby squares until the cruiser has been sunk.